<?php

namespace WPezMaster\App\Core\Admin\Posts\CPT\Register;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

if ( ! class_exists( 'ClassRegister' ) ) {
    class ClassRegister {

        // -- "required" properties
        protected $_str_post_type;


        // -- optional - but (obviously) highly recommended
        protected $_arr_labels;
        protected $_int_menu_position;
        protected $_str_menu_icon;
        protected $_arr_supports;
        protected $_arr_taxonomies;

        protected $_bool_args_unset;
        protected $_arr_args;

        // -- optional

        // TODO x? protected $_str_singular;
        protected $_str_plural;


        // --- __currently__ there are no setters for these. eventually, there will be
        protected $_str_description;
        protected $_bool_public;
        protected $_bool_exclude_from_search;
        protected $_bool_publicly_queryable;
        protected $_bool_show_ui;
        protected $_bool_show_in_nav_menus;
        protected $_bool_show_in_menu;
        protected $_bool_show_in_admin_bar;
        // menu_position
        // menu_icon
        protected $_str_capability_type;
        protected $_bool_map_meta_cap;
        protected $_bool_hierarchical;
        // supports
        protected $_str_register_meta_box_cb;
        // taxonomies
        protected $_bool_has_archive;
        protected $_bool_rewrite;
        protected $_str_permalink_epmask;
        protected $_bool_query_var;
        protected $_bool_can_export;
        protected $_bool_delete_with_user;
        protected $_bool_show_in_rest;
        protected $_str_rest_base;
        protected $_str_rest_controller_class;


        public function __construct( $arr_args = [] ) {

            $this->setPropertyDefaults();
        }

        protected function setPropertyDefaults() {

            $this->_bool_active   = true;
            $this->_str_post_type = false;
            // $this->_str_singular  = false;
            $this->_str_plural = null;

            $this->_arr_labels = [];


            $this->_int_menu_position = 10;
            // https://developer.wordpress.org/resource/dashicons/
            $this->_str_menu_icon  = 'dashicons-controls-play';
            $this->_arr_supports   = [ 'title', 'editor' ];
            $this->_arr_taxonomies = [];

            // ---
            $this->_str_description           = '';
            $this->_bool_public               = false;
            $this->_bool_exclude_from_search  = true;
            $this->_bool_publicly_queryable   = false;
            $this->_bool_show_ui              = false;
            $this->_bool_show_in_nav_menus    = false;
            $this->_bool_show_in_menu         = true; // WP default: fallse
            $this->_bool_show_in_admin_bar    = false;
            $this->_str_capability_type       = 'post';
            $this->_bool_map_meta_cap         = null;
            $this->_bool_hierarchical         = false;
            $this->_str_register_meta_box_cb  = null;
            $this->_bool_has_archive          = false;
            $this->_bool_rewrite              = true;
            $this->_str_permalink_epmask      = EP_PERMALINK;
            $this->_bool_query_var            = true;
            $this->_bool_can_export           = false; // WP default: true
            $this->_bool_delete_with_user     = null;
            $this->_bool_show_in_rest         = false;
            $this->_str_rest_base             = null;
            $this->_str_rest_controller_class = 'WP_REST_Posts_Controller';

            // ---
            $this->_bool_args_unset = true;
            $this->_arr_args        = [];

        }


        public function setActive( $bool = true ) {

            if ( is_bool( $bool ) ) {
                $this->_bool_active = $bool;

                return true;
            }

            return false;
        }

        public function setPostType( $str = false ) {

            if ( is_string( $str ) && strlen( $str ) < 21 ) {
                $this->_str_post_type = $str;

                return true;
            }
            $this->_bool_active = false;

            return false;
        }


        public function setPlural( $str = false ) {

            if ( is_string( $str ) ) {
                $this->_str_plural = $str;

                return true;
            }

            return false;
        }

        public function setLabels( $arr = false ) {

            if ( is_array( $arr ) ) {
                $this->_arr_labels = $arr;

                return true;
            }

            return false;
        }


        public function setMenuPosition( $int = false ) {

            if ( is_integer( $int ) ) {
                $this->_int_menu_position = $int;

                return true;
            }

            return false;
        }

        public function setMenuIcon( $str = false ) {

            if ( is_string( $str ) ) {
                $this->_str_menu_icon = $str;

                return true;
            }

            return false;
        }

        public function setSupports( $arr = false ) {

            if ( is_array( $arr ) ) {
                $this->_arr_supports = $arr;

                return true;
            }

            return false;
        }

        public function setTaxonomies( $arr = false ) {

            if ( is_array( $arr ) ) {

                $this->_arr_taxonomies = $arr;

                return true;
            }

            return false;
        }

        public function setArgsUnset( $bool = true ) {

            if ( is_bool( $bool ) ) {
                $this->_bool_args_unset = $bool;

                return true;
            }

            return false;
        }

        public function setArgs( $arr = false ) {

            if ( is_array( $arr ) ) {
                $this->_arr_args = $arr;

                return true;
            }

            return false;
        }


        /**
         * @return bool
         */
        public function registerPostType() {

            if ( $this->_bool_active === false ) {
                return false;
            }

            // TODO check for min required properties have values

            $this->argsUnset();

            $arr_args = array_merge( $this->argsDefaults(), $this->_arr_args );

            return register_post_type( $this->_str_post_type, $arr_args );

        }


        /**
         * Prevent the args from also overriding the "required" properties
         */
        protected function argsUnset() {

            if ( $this->_bool_args_unset === true ) {

                $arr_unset = [
                    'post_type'     => true,
                    'labels'        => true,
                    'menu_position' => true,
                    'menu_icon'     => true,
                    'supports'      => true,
                    'taxonomies'    => true
                ];

                foreach ( $arr_unset as $str => $bool ) {

                    if ( $bool === true ) {
                        unset( $this->_arr_args[ $str ] );
                    }
                }
            }
        }


        protected function argsDefaults() {

            $args = [

                /*
                 * (string) (optional) A plural descriptive name for the post type marked for translation.
                 *
                 * - Default: Value of $labels['name']
                 */
                'label'                 => $this->_str_plural,

                /*
                 * (array) (optional) labels - An array of labels for this post type. By default, post labels are used
                 * for non-hierarchical post types and page labels for hierarchical ones.
                 *
                 * - Default: if empty, 'name' is set to value of 'label', and 'singular_name' is set to value of 'name'.
                 */
                'labels'                => $this->_arr_labels,

                // (string) (optional) A short descriptive summary of what the post type is.
                // - Default: blank
                'description'           => $this->_str_description,

                /*
                 * (boolean) (optional) Controls how the type is visible to authors (show_in_nav_menus, show_ui) and
                 * readers (exclude_from_search, publicly_queryable).
                 * Default: false
                 *
                 * - 'true' - Implies exclude_from_search: false, publicly_queryable: true, show_in_nav_menus: true,
                 * and show_ui:true. The built-in types attachment, page, and post are similar to this.
                 *
                 * - 'false' - Implies exclude_from_search: true, publicly_queryable: false, show_in_nav_menus: false,
                 * and show_ui: false. The built-in types nav_menu_item and revision are similar to this. Best used
                 * if you'll provide your own editing and viewing interfaces (or none at all).
                 *
                 * If no value is specified for exclude_from_search, publicly_queryable, show_in_nav_menus, or show_ui,
                 * they inherit their values from public.
                 *
                 * - Default: false
                 */
                'public'                => $this->_bool_public,


                /*
                 * (boolean) (importance) Whether to exclude posts with this post type from front end search results.
                 * Default: value of the opposite of public argument
                 *
                 * - 'true' - site/?s=search-term will not include posts of this post type.
                 *
                 * - 'false' - site/?s=search-term will include posts of this post type.
                 *
                 * Note: If you want to show the posts's list that are associated to taxonomy's terms, you must set
                 * exclude_from_search to false (ie : for call site_domaine/?taxonomy_slug=term_slug or site_domaine/taxonomy_slug/term_slug).
                 * If you set to true, on the taxonomy page (ex: taxonomy.php) WordPress will not find your posts and/or pagination
                 * will make 404 error...
                 *
                 * Default: value of the opposite of public argument
                 */
                'exclude_from_search'   => $this->_bool_exclude_from_search,


                /*
                 * (boolean) (optional) Whether queries can be performed on the front end as part of parse_request().
                 * Default: value of public argument
                 *
                 * Note: The queries affected include the following (also initiated when rewrites are handled)
                 * - ?post_type={post_type_key}
                 *
                 * - ?{post_type_key}={single_post_slug}
                 *
                 * - ?{post_type_query_var}={single_post_slug}
                 *
                 * Note: If query_var is empty, null, or a boolean FALSE, WordPress will still attempt to interpret it (4.2.2)
                 * and previews/views of your custom post will return 404s.
                 *
                 * - Default: value of public argument
                 */
                'publicly_queryable'    => $this->_bool_publicly_queryable,


                /*
                 * (boolean) (optional) Whether to generate a default UI for managing this post type in the admin.
                 * Default: value of public argument
                 *
                 * - 'false' - do not display a user-interface for this post type
                 *
                 * - 'true' - display a user-interface (admin panel) for this post type
                 *
                 * Note: _built-in post types, such as post and page, are intentionally set to false.
                 *
                 * - Default: value of public argument
                 */
                'show_ui'               => $this->_bool_show_ui,


                /*
                 * boolean) (optional) Whether post_type is available for selection in (navigation) menus builder.
                 *
                 * - Default: value of public argument
                 */
                'show_in_nav_menus'     => $this->_bool_show_in_nav_menus,


                /*
                 * (boolean or string) (optional) Where to show the post type in the admin menu. show_ui must be true.
                 * Default: value of show_ui argument
                 *
                 * - 'false' - do not display in the admin menu
                 *
                 * - 'true' - display as a top level menu
                 *
                 * - 'some string' - If an existing top level page such as 'tools.php' or 'edit.php?post_type=page', the post type
                 * will be placed as a sub menu of that.
                 *
                 * Note: When using 'some string' to show as a submenu of a menu page created by a plugin, this item will become the first
                 * submenu item, and replace the location of the top-level link. If this isn't desired, the plugin that creates the menu
                 * page needs to set the add_action priority for admin_menu to 9 or lower.
                 *
                 * Note: As this one inherits its value from show_ui, which inherits its value from public, it seems to be the most reliable
                 * property to determine, if a post type is meant to be publicly useable. At least this works for _builtin post types and
                 * only gives back post and page
                 *
                 * - Default: value of show_ui argument
                 */
                'show_in_menu'          => $this->_bool_show_in_menu, // false,


                /*
                 * (boolean) (optional) Whether to make this post type available in the WordPress admin bar.
                 *
                 * - Default: value of the show_in_menu argument
                 */
                'show_in_admin_bar'     => $this->_bool_show_in_admin_bar,


                /*
                 * (integer) (optional) The position in the menu order the post type should appear. show_in_menu must be true.
                 * Default: null - defaults to below Comments
                 *
                 * 5 - below Posts
                 * 10 - below Media
                 * 15 - below Links
                 * 20 - below Pages
                 * 25 - below comments
                 * 60 - below first separator
                 * 65 - below Plugins
                 * 70 - below Users
                 * 75 - below Tools
                 * 80 - below Settings
                 * 100 - below second separator
                 *
                 * - Default: null - defaults to below Comments
                 */
                'menu_position'         => $this->_int_menu_position,


                /*
                 * (string) (optional) The url to the icon to be used for this menu or the name of the icon from the iconfont [1]
                 * Default: null - defaults to the posts icon
                 *
                 * Examples
                 * - 'dashicons-video-alt' (Uses the video icon from Dashicons[2])
                 * 'get_template_directory_uri() . "/images/cutom-posttype-icon.png"' (Use a image located in the current theme)
                 */
                'menu_icon'             => $this->_str_menu_icon,


                /*
                 * (string or array) (optional) The string to use to build the read, edit, and delete capabilities. May be passed as
                 * an array to allow for alternative plurals when using this argument as a base to construct the capabilities,
                 * e.g. array('story', 'stories') the first array element will be used for the singular capabilities and the second
                 * array element for the plural capabilities, this is instead of the auto generated version if no array is given which
                 * would be "storys". The 'capability_type' parameter is used as a base to construct capabilities unless they are
                 * explicitly set with the 'capabilities' parameter. It seems that `map_meta_cap` needs to be set to false or null,
                 * to make this work (see note 2 below).
                 *
                 * For examples: https://codex.wordpress.org/Function_Reference/register_post_type#Taxonomies
                 *
                 * - Default: "post"
                 */
                'capability_type'       => $this->_str_capability_type,

                /*
                 * (array) (optional) An array of the capabilities for this post type.
                 *
                 * For examples: https://codex.wordpress.org/Function_Reference/register_post_type#Taxonomies
                 *
                 * - Default: capability_type is used to construct
                 */
                //'capabilities' => array(),

                /*
                 * (boolean) (optional) Whether to use the internal default meta capability handling.
                 *
                 * - Default: null
                 */
                'map_meta_cap'          => $this->_bool_map_meta_cap,


                /*
                 * (boolean) (optional) Whether the post type is hierarchical (e.g. page). Allows Parent to be specified.
                 * The 'supports' parameter should contain 'page-attributes' to show the parent select box on the editor page.
                 *
                 * - Default: false
                 */
                'hierarchical'          => $this->_bool_hierarchical,

                /*
                 * (array/boolean) (optional) An alias for calling add_post_type_support() directly.
                 *
                 * As of 3.5, boolean false can be passed as value instead of an array to prevent default (title and editor) behavior.
                 *
                 * Default: title and editor
                 *
                 * - 'title'
                 * - 'editor' (content)
                 * - 'author'
                 * - 'thumbnail' (featured image, current theme must also support post-thumbnails)
                 * - 'excerpt'
                 * - 'trackbacks'
                 * - 'custom-fields'
                 * - 'comments' (also will see comment count balloon on edit screen)
                 * - 'revisions' (will store revisions)
                 * - 'page-attributes' (menu order, hierarchical must be true to show Parent option)
                 * - 'post-formats' add post formats, see Post Formats
                 *
                 * attay('title', 'editor', 'author', 'thumbnail', 'excerpt', 'trackbacks', 'custom-fields', 'comments', 'revisions', 'page-attributes', 'post-formats')
                 *
                 * Note: When you use custom post type that use thumbnails remember to check that the theme also supports thumbnails
                 * or use add_theme_support function.
                 */
                'supports'              => $this->_arr_supports,

                /*
                 * (callback ) (optional) Provide a callback function that will be called when setting up the meta boxes for the edit form.
                 * The callback function takes one argument $post, which contains the WP_Post object for the currently edited post.
                 * Do remove_meta_box() and add_meta_box() calls in the callback.
                 *
                 * - Default: None
                 */
                'register_meta_box_cb'  => $this->_str_register_meta_box_cb,

                /*
                 * (array) (optional) An array of registered taxonomies like category or post_tag that will be used with this post type.
                 * This can be used in lieu of calling register_taxonomy_for_object_type() directly. Custom taxonomies still need to be
                 * registered with register_taxonomy().
                 *
                 * - Default: no taxonomies
                 */
                'taxonomies'            => $this->_arr_taxonomies,


                /*
                 * (boolean or string) (optional) Enables post type archives. Will use $post_type as archive slug by default.
                 * Default: false
                 *
                 * Note: Will generate the proper rewrite rules if rewrite is enabled. Also use rewrite to change the slug used.
                 * If string, it should be translatable.
                 *
                 * - Default: false
                 */
                'has_archive'           => $this->_bool_has_archive,


                /*
                 * (boolean or array) (optional) Triggers the handling of rewrites for this post type. To prevent rewrites, set to false.
                 * Default: true and use $post_type as slug
                 *
                 * $args array
                 * - 'slug' => string Customize the permalink structure slug. Defaults to the $post_type value. Should be translatable.
                 * - 'with_front' => bool Should the permalink structure be prepended with the front base. (example: if your permalink structure is /blog/, then your links will be: false->/news/, true->/blog/news/). Defaults to true
                 * - 'feeds' => bool Should a feed permalink structure be built for this post type. Defaults to has_archive value.
                 * - 'pages' => bool Should the permalink structure provide for pagination. Defaults to true
                 * - 'ep_mask' => const As of 3.4 Assign an endpoint mask for this post type (example). For more info see Rewrite API/add_rewrite_endpoint, and Make WordPress Plugins summary of endpoints.
                 * ----If not specified, then it inherits from permalink_epmask(if permalink_epmask is set), otherwise defaults to EP_PERMALINK.
                 *
                 * Note: If registering a post type inside of a plugin, call flush_rewrite_rules() in your activation and deactivation hook
                 * (see Flushing Rewrite on Activation below). If flush_rewrite_rules() is not used, then you will have to manually go to Settings > Permalinks and refresh your permalink structure before your custom post type will show the correct structure.
                 *
                 * - Default: true and use $post_type as slug
                 */
                'rewrite'               => $this->_bool_rewrite,

                /*
                 * (string) (optional) The default rewrite endpoint bitmasks. For more info see Trac Ticket 12605 and this - Make WordPress
                 * Plugins summary of endpoints.
                 * Default: EP_PERMALINK
                 *
                 * Note: In 3.4, this argument is effectively replaced by the 'ep_mask' argument under rewrite.
                 *
                 * - Default: EP_PERMALINK
                 */
                'permalink_epmask'      => $this->_str_permalink_epmask,


                /*
                 * (boolean or string) (optional) Sets the query_var key for this post type.
                 * Default: true - set to $post_type
                 *
                 * - 'false' - Disables query_var key use. A post type cannot be loaded at /?{query_var}={single_post_slug}
                 * - 'string' - /?{query_var_string}={single_post_slug} will work as intended.
                 *
                 * Note: The query_var parameter has no effect if the ‘publicly_queryable’ parameter is set to false. query_var adds the
                 * custom post type’s query var to the built-in query_vars array so that WordPress will recognize it. WordPress removes
                 * any query var not included in that array.
                 *
                 * If set to true it allows you to request a custom posts type (book) using this: example.com/?book=life-of-pi
                 *
                 * If set to a string rather than true (for example ‘publication’), you can do: example.com/?publication=life-of-pi
                 *
                 * Default: true - set to $post_type
                 */
                'query_var'             => $this->_bool_query_var,

                /*
                 * (boolean) (optional) Can this post_type be exported.
                 *
                 * - Default: true BUT we default to false.
                 */
                'can_export'            => $this->_bool_can_export,

                /*
                 * (boolean) (optional) Whether to delete posts of this type when deleting a user. If true, posts of this type belonging
                 * to the user will be moved to trash when then user is deleted. If false, posts of this type belonging to the user will
                 * not be trashed or deleted. If not set (the default), posts are trashed if post_type_supports('author'). Otherwise posts
                 * are not trashed or deleted.
                 *
                 * - Default: null
                 */
                'delete_with_user'      => $this->_bool_delete_with_user,

                /*
                 * (boolean) (optional) Whether to expose this post type in the REST API.
                 *
                 * - Default: false
                 */
                'show_in_rest'          => $this->_bool_show_in_rest,

                /*
                 * (string) (optional) The base slug that this post type will use when accessed using the REST API.
                 *
                 * Default: $post_type
                 */
                'rest_base'             => $this->_str_rest_base,

                /*
                 * (string) (optional) An optional custom controller to use instead of WP_REST_Posts_Controller.
                 * Must be a subclass of WP_REST_Controller.
                 */
                'rest_controller_class' => $this->_str_rest_controller_class,

                /*
                 * (boolean) (not for general use) Whether this post type is a native or "built-in" post_type.
                 *
                 * Note: this Codex entry is for documentation - core developers recommend you don't use this when registering your own post type
                 *
                 * Default: false
                 *
                 * - 'false' - default this is a custom post type
                 * - 'true' - this is a built-in native post type (post, page, attachment, revision, nav_menu_item)
                 *
                 * - Default: false
                 */
                // '_builtin' => false,


                /*
                 * (boolean) (not for general use) Link to edit an entry with this post type.
                 *
                 * Note: this Codex entry is for documentation - core developers recommend you don't use this when registering your own post type
                 *
                 * Default: 'post.php?post=%d'
                 */
                // '_edit_link' => false,

            ];

            return $args;
        }

    }
}