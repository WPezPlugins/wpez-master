<?php

namespace WPezMaster\App\Core\Admin\Posts\Taxonomy\Labels;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

if ( ! class_exists( 'ClassEnglish' ) ) {
    class ClassEnglish {
        
        protected $_str_singular;
        protected $_str_plural;

        public function __construct( $arr_args = array() ) {

            $this->setPropertyDefaults();

        }

        protected function setPropertyDefaults(){
            
            $this->_str_singular = false;
            $this->_str_str_plural = false;

        }

        public function setSingular( $str = false ){

            $str = esc_attr($str);
            if ( is_string($str) && ! empty( $str ) ){
                $this->_str_singular = $str;
                return true;
            }
            return false;
        }

        public function setPlural( $str = false ){
            
            $str = esc_attr($str);
            if ( is_string($str) && ! empty( $str ) ){
                $this->_str_plural = $str;
                return true;
            }
            return false;
        }

        function getLabels() {
            
            
            if ( $this->_str_singular === false){
                return [];
            }

            if ( $this->_str_plural === false ){
                $this->_str_plural = $this->_str_singular . 's';
            }

            // https://codex.wordpress.org/Function_Reference/register_taxonomy

            $arr_labels = array(
                // 'name' - general name for the taxonomy, usually plural. The same as and overridden by $tax->label. Default is _x( 'Post Tags', 'taxonomy general name' ) or _x( 'Categories', 'taxonomy general name' ).
                'name'                  => $this->_str_plural,

                // 'singular_name' - name for one object of this taxonomy. Default is _x( 'Post Tag', 'taxonomy singular name' ) or _x( 'Category', 'taxonomy singular name' ).
                'singular_name'         => $this->_str_singular,

                // 'menu_name' - the menu name text. This string is the name to give menu items. If not set, defaults to value of name label.
                'menu_name' => $this->_str_plural,

                // 'all_items' - the all items text. Default is __( 'All Tags' ) or __( 'All Categories' )
                'all_items' => 'All' . ' ' . $this->_str_plural,

                // 'edit_item' - the edit item text. Default is __( 'Edit Tag' ) or __( 'Edit Category' )
                'edit_item'             => 'Edit' . ' ' . $this->_str_singular,

                // 'view_item' - the view item text, Default is __( 'View Tag' ) or __( 'View Category' )
                'view_item'             => 'View' . ' ' . $this->_str_singular,

                // 'update_item' - the update item text. Default is __( 'Update Tag' ) or __( 'Update Category' )
                'update_item'             => 'Update' . ' ' . $this->_str_singular,

                // add_new_item' - the add new item text. Default is __( 'Add New Tag' ) or __( 'Add New Category' )
                'add_new_item'             => 'Add New' . ' ' . $this->_str_singular,

                // 'new_item_name' - the new item name text. Default is __( 'New Tag Name' ) or __( 'New Category Name' )
                'new_item_name'             => 'New' . ' ' . $this->_str_singular . ' ' . 'Name',

                // 'parent_item' - the parent item text. This string is not used on non-hierarchical taxonomies such as post tags. Default is null or __( 'Parent Category' )
                'parent_item' =>  'Parent' . ' ' . $this->_str_singular,

                // 'parent_item_colon' - The same as parent_item, but with colon : in the end null, __( 'Parent Category:' )
                'parent_item_colon'     => 'Parent' . ' '. $this->_str_singular . ':',

                // 'search_items' - the search items text. Default is __( 'Search Tags' ) or __( 'Search Categories' )
                'search_items'          => 'Search' . ' ' . $this->_str_plural,

                // 'popular_items' - the popular items text. This string is not used on hierarchical taxonomies. Default is __( 'Popular Tags' ) or null
                'popular_items' => 'Popular' . ' ' . $this->_str_plural,

                // 'separate_items_with_commas' - the separate item with commas text used in the taxonomy meta box. This string is not used on hierarchical taxonomies. Default is __( 'Separate tags with commas' ), or null
                'separate_items_with_commas' =>  'Separate' . ' '. strtolower($this->_str_plural).  ' ' . 'with commas',

                // 'add_or_remove_items' - the add or remove items text and used in the meta box when JavaScript is disabled. This string is not used on hierarchical taxonomies. Default is __( 'Add or remove tags' ) or null
                'add_or_remove_items' => 'Add or remove' . ' ' . strtolower($this->_str_plural),

                // 'choose_from_most_used' - the choose from most used text used in the taxonomy meta box. This string is not used on hierarchical taxonomies. Default is __( 'Choose from the most used tags' ) or null
                'choose_from_most_used' => 'Choose from the most used' . ' ' . strtolower($this->_str_plural),

                // 'not_found' (3.6+) - the text displayed via clicking 'Choose from the most used tags' in the taxonomy meta box when no tags are available and (4.2+) - the text used in the terms list table when there are no items for a taxonomy. Default is __( 'No tags found.' ) or __( 'No categories found.' )
                'not_found'             => 'Sorry. Search found no' . ' ' . $this->_str_plural,

            );

            return $arr_labels;
        }

    }
}