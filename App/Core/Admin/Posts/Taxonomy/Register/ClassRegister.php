<?php

namespace WPezMaster\App\Core\Admin\Posts\Taxonomy\Register;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

if ( ! class_exists( 'ClassRegister' ) ) {
    class ClassRegister {

        protected $_bool_active;

        // -- "required" properties

        // The name of the taxonomy. Name should only contain lowercase letters and the underscore character, and not be more than 32 characters long (database structure restriction).
        protected $_str_taxonomy;
        protected $_str_plural;
        protected $_mix_object_type;

        // -- optional - but (obviously) highly recommended
        protected $_str_label;
        protected $_arr_labels;

        // setters - or use the setArgs();
        protected $_bool_public;
        protected $_bool_publicly_queryable;
        protected $_bool_show_ui;
        protected $_bool_show_in_menu;
        protected $_bool_show_in_nav_menus;
        protected $_bool_show_in_rest;

        protected $_str_rest_base;
        protected $_str_rest_controller_class;

        protected $_bool_show_tagcloud;
        protected $_bool_show_in_quick_edit;

        protected $_str_meta_box_cb;

        protected $_bool_show_admin_column;

        protected $_str_description;

        protected $_bool_hierarchical;

        protected $_str_update_count_callback;
        protected $_mix_query_var;
        protected $_mix_rewrite;
        protected $_arr_capabilities;

        protected $_bool_sort;

        protected $_bool_builtin;

        // --- __currently__ there are no setters for these. eventually, there will be


        public function __construct( $arr_args = [] ) {

            $this->setPropertyDefaults();
        }

        protected function setPropertyDefaults() {

            $this->_bool_active = false;

            $this->_str_taxonomy = false;

            $this->_str_plural = false;

            $this->_mix_object_type = null;

            $this->_arr_labels = [];

            // ---

            $this->_bool_public = true;

            $this->_bool_publicly_queryable = null;

            $this->_bool_show_ui = null;

            $this->_bool_show_in_menu = null;

            $this->_bool_show_in_nav_menus = null;

            $this->_bool_show_in_rest = false;

            $this->_str_rest_base = null;

            $this->_str_rest_controller_class = 'WP_REST_Terms_Controller';

            $this->_bool_show_tagcloud = null;

            $this->_bool_show_in_quick_edit = null;

            $this->_str_meta_box_cb = null;

            $this->_bool_show_admin_column = false;

            $this->_str_description = '';

            $this->_bool_hierarchical = false;

            $this->_str_update_count_callback = null;

            $this->_mix_query_var = null;

            $this->_mix_rewrite = true;

            $this->_arr_capabilities = [];

            $this->_bool_sort = null;

            $this->_bool_builtin = false;

        }


        public function setActive( $bool = true ) {

            return $this->setBool( '_bool_active', $bool );
        }

        public function setTaxonomy( $str = false ) {

            if ( is_string( $str ) && strlen( $str ) < 33 ) {
                $this->_str_taxonomy = $str;
                $this->_bool_active  = true;

                return true;
            }
            $this->_bool_active = false;

            return false;
        }

        public function setPlural( $str = false ) {

            if ( is_string( $str ) ) {
                $this->_str_plural = $str;

                return true;
            }

            return false;
        }

        public function setObjectType( $mix = false ) {

            
            if ( ( is_array( $mix ) && ! empty( $mix ) ) || is_string('$mix') ) {

                $this->_mix_object_type = $mix;

                return true;
            }

            return false;
        }


        public function setLabels( $arr = false ) {

            if ( is_array( $arr ) ) {
                $this->_arr_labels = $arr;

                return true;
            }

            return false;
        }


        public function setArgs( $arr = false ) {

            if ( is_array( $arr ) ) {
                $this->_arr_args = $arr;

                return true;
            }

            return false;
        }

        public function setPublic( $bool = true ) {

            return $this->setBool( '_bool_public', $bool );
        }

        public function setPubliclyQueryable( $bool = null ) {

            return $this->setBool( '_bool_publicly_queryable', $bool );
        }

        public function setShowUI( $bool = null ) {

            return $this->setBool( '_bool_show_ui', $bool );
        }

        public function setShowInMenu( $bool = null ) {

            return $this->setBool( '_bool_show_in_menu', $bool );
        }

        public function setShowInNavMenus( $bool = null ) {

            return $this->setBool( '_bool_show_in_nav_menus', $bool );
        }

        public function setShowInRest( $bool = null ) {

            return $this->setBool( '_bool_show_in_rest', $bool );
        }

        public function setRestBase( $str = null ) {

            return $this->setString( '_str_rest_base', $str );
        }

        public function setRestControllerClass( $str = false ) {

            return $this->setString( '_str_rest_controller_class', $str );
        }


        public function setShowTagcloud( $bool = null ) {

            return $this->setBool( '_bool_show_tagcloud', $bool );
        }

        public function setShowInQuickEdit( $bool = null ) {

            return $this->setBool( '_bool_show_in_quick_edit', $bool );
        }

        public function setMetaBoxCB( $str = null ) {

            return $this->setString( '_str_meta_box_cb', $str );
        }


        public function setShowAdminColumn( $bool = false ) {

            return $this->setBool( '_bool_show_admin_column', $bool );
        }


        public function setDescription( $str = '' ) {

            return $this->setString( '_str_description', $str );
        }


        public function setHierarchical( $bool = false ) {

            return $this->setBool( '_bool_hierarchical', $bool );
        }

        public function setUpdateCountCallback( $str = null ) {

            return $this->setString( '_str_update_count_callback', $str );
        }

        public function setQueryVar( $mix = null ) {

            if ( is_bool( $mix ) || is_string( $mix ) ) {
                $this->_mix_query_var = $mix;

                return true;
            }

            return false;
        }

        public function setRewrite( $mix = true ) {

            if ( is_bool( $mix ) || is_array( $mix ) ) {
                $this->_mix_query_var = $mix;

                return true;
            }

            return false;
        }

        public function setCapabilities( $arr = false ) {

            if ( is_array( $arr ) ) {
                $this->_arr_capabilities = $arr;

                return true;
            }

            return false;
        }

        public function setSort( $bool = null ) {

            return $this->setBool( '_bool_sort', $bool );
        }

        public function setBuiltIn( $bool = false ) {

            return $this->setBool( '_bool_builtin', $bool );
        }


        protected function setBool( $str_prop = false, $bool = '' ) {

            if ( $str_prop !== false && is_bool( $bool ) ) {
                $this->$str_prop = $bool;

                return true;
            }

            return false;
        }

        protected function setString( $str_prop = false, $str = false ) {

            if ( $str_prop !== false && is_string( $str ) ) {
                $this->$str_prop = $str;

                return true;
            }

            return false;
        }


        /**
         * @param bool $bool
         *
         * @return bool|void|\WP_Error
         */
        public function registerTaxonomy( $bool = true ) {

            if ( $this->_bool_active === false || $bool === false ) {
                return false;
            }

            if ( $this->_str_plural === false ) {
                $this->_str_plural = $this->_str_taxonomy;
            }

            // TODO check for min required properties have values
            // $this->argsUnset();

            $arr_args = array_merge( $this->argsDefaults(), $this->_arr_args );

            // Codex: "True is not seen as a valid entry and will result in 404 issues."
            if ( $arr_args['query_var'] === true ) {
                $arr_args['query_var'] = null;
            }

            return register_taxonomy( $this->_str_taxonomy, $this->_mix_object_type, $arr_args );
        }


        /**
         * Prevent the args from also overriding the "required" properties
         */
        protected function argsUnset() {

            if ( $this->_bool_args_unset === true ) {

                $arr_unset = [
                    //TODO
                ];

                foreach ( $arr_unset as $str => $bool ) {

                    if ( $bool === true ) {
                        unset( $this->_arr_args[ $str ] );
                    }
                }
            }
        }


        protected function argsDefaults() {

            $args = [

                /*
                 * (string) (optional) A plural descriptive name for the post type marked for translation.
                 *
                 * - Default: Value of $labels['name']
                 */
                'label'                 => $this->_str_plural,

                /*
                 * (array) (optional) labels - An array of labels for this post type. By default, post labels are used
                 * for non-hierarchical post types and page labels for hierarchical ones.
                 *
                 * - Default: if empty, 'name' is set to value of 'label', and 'singular_name' is set to value of 'name'.
                 */
                'labels'                => $this->_arr_labels,

                /*
                 * (boolean) (optional) Whether a taxonomy is intended for use publicly either via the admin interface
                 * or by front-end users. The default settings of `$publicly_queryable`, `$show_ui`, and `$show_in_nav_menus` are inherited from `$public`.
                 */
                'public'                => $this->_bool_public,

                // (boolean) (optional) Whether the taxonomy is publicly queryable.
                'publicly_queryable'    => $this->_bool_publicly_queryable,

                // (boolean) (optional) Whether to generate a default UI for managing this taxonomy.
                // Default: if not set, defaults to value of public argument. As of 3.5, setting this to false for attachment taxonomies will hide the UI.
                'show_ui'               => $this->_bool_show_ui,

                // (boolean) (optional) Where to show the taxonomy in the admin menu. show_ui must be true.
                // Default: value of show_ui argument
                // false - do not display in the admin menu
                // true - show as a submenu of associated object types
                'show_in_menu'          => $this->_bool_show_in_menu,

                // (boolean) (optional) true makes this taxonomy available for selection in navigation menus.
                // Default: if not set, defaults to value of public argument
                'show_in_nav_menus'     => $this->_bool_show_in_nav_menus,

                // (boolean) (optional) Whether to include the taxonomy in the REST API.
                // Default: false
                'show_in_rest'          => $this->_bool_show_in_rest,

                // (string) (optional) To change the base url of REST API route.
                // Default: $taxonomy
                'rest_base'             => $this->_str_rest_base,

                // (string) (optional) REST API Controller class name.
                // Default: WP_REST_Terms_Controller
                'rest_controller_class' => $this->_str_rest_controller_class,

                // (boolean) (optional) Whether to allow the Tag Cloud widget to use this taxonomy.
                // Default: if not set, defaults to value of show_ui argument
                'show_tagcloud'         => $this->_bool_show_tagcloud,

                // (boolean) (optional) Whether to show the taxonomy in the quick/bulk edit panel. (Available since 4.2)
                //Default: if not set, defaults to value of show_ui argument
                'show_in_quick_edit'    => $this->_bool_show_in_quick_edit,

                // (callback) (optional) Provide a callback function name for the meta box display. (Available since 3.8)
                // Default: null
                // Note: Defaults to the categories meta box (post_categories_meta_box() in meta-boxes.php) for hierarchical taxonomies and the
                // tags meta box (post_tags_meta_box()) for non-hierarchical taxonomies. No meta box is shown if set to false.
                'meta_box_cb'           => $this->_str_meta_box_cb,

                // (boolean) (optional) Whether to allow automatic creation of taxonomy columns on associated post-types table. (Available since 3.5)
                // Default: false
                'show_admin_column'     => $this->_bool_show_admin_column,

                // (string) (optional) Include a description of the taxonomy.
                // Default: ""
                'description'           => $this->_str_description,

                // (boolean) (optional) Is this taxonomy hierarchical (have descendants) like categories or not hierarchical like tags.
                // Default: false
                // Note: Hierarchical taxonomies will have a list with checkboxes to select an existing category in the taxonomy admin box on the post edit page (like default post categories). Non-hierarchical taxonomies will just have an empty text field to type-in taxonomy terms to associate with the post (like default post tags).
                'hierarchical'          => $this->_bool_hierarchical,

                // (string) (optional) A function name that will be called when the count of an associated $object_type, such as post, is updated. Works much like a hook.
                // Default: None - but see Note
                // Note: For all details please see: https://codex.wordpress.org/Function_Reference/register_taxonomy
                'update_count_callback' => $this->_str_update_count_callback,

                // (boolean or string) (optional) False to disable the query_var, set as string to use custom query_var instead of default which is $taxonomy, the taxonomy's "name". True is not seen as a valid entry and will result in 404 issues.
                // Default: $taxonomy
                // Note: The query_var is used for direct queries through WP_Query like new WP_Query(array('people'=>$person_name)) and URL queries
                // like /?people=$person_name. Setting query_var to false will disable these methods, but you can still fetch posts with an
                // explicit WP_Query taxonomy query like WP_Query(array('taxonomy'=>'people', 'term'=>$person_name)).
                'query_var'             => $this->_mix_query_var,

                // (boolean/array) (optional) Set to false to prevent automatic URL rewriting a.k.a. "pretty permalinks". Pass an $args array to override default URL settings for permalinks as outlined below:
                // Default: true
                // Note: For all details please see: https://codex.wordpress.org/Function_Reference/register_taxonomy
                'rewrite'               => $this->_mix_rewrite,

                // (array) (optional) An array of the capabilities for this taxonomy.
                // Default: None
                'capabilities'          => $this->_arr_capabilities,

                // (boolean) (optional) Whether this taxonomy should remember the order in which terms are added to objects.
                // Default: None
                'sort'                  => $this->_bool_sort,

                // (boolean) (not for general use) Whether this taxonomy is a native or "built-in" taxonomy. Note: this Codex entry is for documentation - core developers recommend you don't use this when registering your own taxonomy
                // Default: false
                '_builtin'              => $this->_bool_builtin,
            ];

            return $args;
        }

    }
}