<?php

namespace WPezMaster\App\Core\Traits\MarkupGeneration;


trait TraitMarkupGeneration {

    protected $_mg_ele_attrs_boolean    = [];
    protected $_mg_html_ir_boolean      = [ 'disabled', 'readonly', 'required' ];
    protected $_mg_html_attrs_boolean   = [ 'autofocus', 'formnovalidate', 'multiple', 'required' ];
    protected $_mg_global_attrs_boolean = [ 'hidden', 'itemscope' ];


    /**
     * @param string $str_ele
     * @param string $arr_gats
     * @param bool   $mix_validation << TODO
     *
     * @return string
     */
    protected function tagOpen( $str_ele = '', $arr_gats = '', $mix_validation = false ) {

        $str_ele_esc = esc_attr( $str_ele );

        $str_ret = '';
        // TODO - element tag validation?
        if ( ! empty( $str_ele_esc ) ) {
            $str_gats = $this->globalAttrs( $arr_gats );
            $str_ret  .= '<' . $str_ele_esc . $str_gats . '>';
        }

        return $str_ret;
    }


    /**
     * @param string $str_ele
     * @param string $arr_gats
     * @param bool   $mix_validation
     *
     * @return string
     */
    protected function tagClose( $str_ele = '', $arr_gats = '', $mix_validation = false ) {

        $str_ele_esc = esc_attr( $str_ele );

        $str_ret = '';
        // TODO - element tag validation?

        if ( ! empty( $str_ele_esc ) ) {
            $str_ret .= '</' . $str_ele_esc . '>';
        }

        return $str_ret;
    }

    /**
     * ref: https://www.w3schools.com/html/html_form_input_types.asp Section:
     * Input Restrictions
     *
     * @param $arr_args
     *
     * @return string
     */
    protected function htmlIR( $arr_attrs = [], $bool_unset = true ) {

        if ( is_array( $arr_attrs ) && ! empty( $arr_attrs ) ) {

            // a slight - temporary? - deviation from standards
            if ( $bool_unset && isset( $arr_attrs['value'] ) ) {
                unset( $arr_attrs['value'] );
            }

            return $this->attrsImplode( $arr_attrs, $this->_mg_html_ir_boolean );
        }

        return '';
    }

    /**
     * @param      $arr_attrs
     * @param bool $bool_unset
     *
     * @return string
     */
    protected function htmlAttrs( $arr_attrs, $bool_unset = true ) {

        if ( is_array( $arr_attrs ) && ! empty( $arr_attrs ) ) {

            return $this->attrsImplode( $arr_attrs, $this->_mg_html_attrs_boolean );
        }

        return '';
    }


    /**
     * @param $arr_attrs
     *
     * @return string
     */
    protected function eleAttrs( $arr_attrs ) {

        if ( is_array( $arr_attrs ) && ! empty( $arr_attrs ) ) {

            return ' ' . $this->attrsImplode( $arr_attrs, $this->_mg_ele_attrs_boolean );
        }

        return '';
    }


    /**
     * @param $arr_attrs
     *
     * @return string
     */
    protected function globalAttrs( $arr_attrs ) {

        if ( is_array( $arr_attrs ) && ! empty( $arr_attrs ) ) {

            return ' ' . $this->attrsImplode( $arr_attrs, $this->_mg_global_attrs_boolean );
        }

        return '';
    }

    /**
     * @param $arr_html
     * @param $arr_bool_unset
     *
     * @return string
     */
    protected function attrsImplode( $arr_html, $arr_bool_unset ) {

        $arr_temp = [];
        foreach ( $arr_bool_unset as $key ) {
            if ( isset( $arr_html[ $key ] ) ) {
                $arr_temp[] = $key;
                unset( $arr_html[ $key ] );
            }
        }

        foreach ( $arr_html as $key => $val ) {
            $arr_temp[] = sanitize_key( $key ) . '="' . esc_attr( $val ) . '"';
        }

        return implode( ' ', $arr_temp );
    }

    /**
     * Send in an arr of attr arrs and return all the attr types (instead of
     * one at a time)
     *
     * @param      $arr_field
     * @param bool $bool_str_ret
     *
     * @return array|bool|string
     */
    protected function allAttrs( $arr_field, $bool_str_ret = true ) {

        if ( ! is_array( $arr_field ) ) {
            return false;
        }

        $arr_ret = [];
        foreach ( $this->allAttrsDefaults() as $key => $val ) {

            if ( isset( $arr_field[ $key ] ) && $val !== false ) {
                // for each key (e.g., 'eleAttrs') do that method()

                // TODO - test for method exists
                $arr_ret[ $key ] = $this->$key( $arr_field[ $key ] );
            }
        }
        // return the arr (of strings), without imploding. perhaps you need to do something before imploding?
        // for example, you really didn't want *all* but using this method was ez'ier ;)
        if ( $bool_str_ret !== true ) {
            return $arr_ret;
        }

        $arr_vals = array_values( $arr_ret );
        $str_ret  = implode( ' ', $arr_vals );

        return $str_ret;
    }


    protected function allAttrsDefaults() {

        $arr_defs = [
            'eleAttrs'    => true,
            'globalAttrs' => true,
            'htmlAttrs'   => true,
            'htmlIR'      => true
        ];

        return $arr_defs;
    }


    /**
     * Note: Nothing dictates that the semantic wrap has to be semantic. But if
     * it's used, it probably would/should be
     *
     * @param string $obj_vwrap
     * @param bool   $bool_wrapper
     *
     * @return \stdClass
     */
    public function viewWrapper( $obj_vwrap = '', $bool_wrapper = true ) {

        /*
         * every view can have an enclose (i.e., semantic wrapper* and a wrapper nested within that).
         * (1) this standardizes it. it's (almost) always there. we can count on being
         *     able to customize an enclose for any given a view
         * (2) it's one less thing for the view dev to worry about.
         * (3) that is, it "forces" the view to be as minimal as possible. beyond that,
         *     the enclose_setup() takes care of what is (almost) always there.
         */

        $arr_vwrap = [ 'semantic', 'view_wrapper' ];

        $obj_ret = new \stdClass();

        $obj_ret->semantic_open      = '';
        $obj_ret->semantic_close     = '';
        $obj_ret->view_wrapper_open  = '';
        $obj_ret->view_wrapper_close = '';


        if ( $bool_wrapper === false || ! is_object( $obj_vwrap ) || ( isset( $obj_vwrap->active ) && $obj_vwrap->active === false ) ) {
            return $obj_ret;
        }

        foreach ( $arr_vwrap as $key => $str_val ) {

            $str_val    = trim( $str_val );
            $str_active = trim( $str_val ) . '_active';

            if ( ! isset( $obj_vwrap->$str_active ) || $obj_vwrap->$str_active !== false ) {

                $str_tag  = $str_val . '_tag';
                $str_gats = $str_val . '_global_attrs';

                // set the properties for this enclose value
                $str_open            = $str_val . '_open';
                $str_close           = $str_val . '_close';
                $obj_ret->$str_open  = $this->tagOpen( $obj_vwrap->$str_tag, $obj_vwrap->$str_gats );
                $obj_ret->$str_close = $this->tagClose( $obj_vwrap->$str_tag );
            }
        }

        return $obj_ret;
    }
}