<?php

namespace WPezMaster\App\Core\Traits\Set;

trait TraitSetPostType {

    protected function setPostType( $str_prop = false, $str_post_type = false ) {

        if ( post_type_exists( $str_post_type)) {

            $this->$str_prop = $str_post_type;

            return true;
        }
        return false;
    }
}