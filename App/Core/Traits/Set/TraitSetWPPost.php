<?php
/**
 * Created by PhpStorm.
 */

namespace WPezMaster\App\Core\Traits\Set;

trait TraitSetWPPost {

    protected function setWPPost( $str_prop = false, $obj = false ) {

        if ( property_exists( $this, $str_prop ) && $obj instanceof \WP_Post) {

            $this->$str_prop = $obj;

            return true;
        }
        return false;
    }
}