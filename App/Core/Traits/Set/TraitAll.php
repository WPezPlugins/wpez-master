<?php
namespace WPezMaster\App\Core\Traits\Set;

trait TraitAll {

    use \WPezMaster\App\Core\Traits\Set\TraitSetArray;
    use \WPezMaster\App\Core\Traits\Set\TraitSetBool;
    use \WPezMaster\App\Core\Traits\Set\TraitSetPostType;
    use \WPezMaster\App\Core\Traits\Set\TraitSetString;
    use \WPezMaster\App\Core\Traits\Set\TraitSetWPPost;

}