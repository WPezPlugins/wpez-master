<?php


namespace WPezMaster\App\Core\Traits\Helpers\SavePost;

trait TraitCheckSavePost {

    /**
     * The save_post action has fired...but is it a "legit" save or not?
     *
     * @param bool   $post
     * @param int    $str_action
     * @param string $str_nonce_name
     *
     * @return bool
     */
    protected function checkSavePost( $post = false , $str_action = -1, $str_nonce_name = '_wpnonce'){

        if ( ! $post instanceof \WP_Post ){
            return true;
        }
        /* Verify the nonce before proceeding. */
        if ( ! isset( $_POST[$str_nonce_name] ) || ! wp_verify_nonce( $_POST[$str_nonce_name], $str_action ) ) {
            return true;
        }

        /* Get the post type object. */
        $post_type = get_post_type_object( $post->post_type );

        /* Check if the current user has permission to edit the post. */
        if ( ! current_user_can( $post_type->cap->edit_post, $post->ID ) ) {
            return true;
        }

        if ( defined( "DOING_AUTOSAVE" ) && DOING_AUTOSAVE ) {
            return true;
        }
    }
}