<?php
/**
 * Created by PhpStorm.
 */


namespace WPezMaster\App\Core\Traits\Helpers\URL;

trait TraitURLStrip {

    /**
     * Strips off paging and query string. Returns a filter-the-results friendly URL
     *
     * @param bool   $str_url
     * @param string $str_page
     *
     * @return bool|string
     */
    protected function urlStrip( $str_url = false , $str_page = 'page' ){

        if ( is_string( $str_url) ) {

            $arr_parse_url = wp_parse_url( $str_url );

            $str_path_no_page = '';
            if ( isset( $arr_parse_url['path'] ) ) {

                // if we're paging then remove that. please!
                $str_path_no_page = preg_replace( "/\/{$str_page}\/[0-9]*\/$/", "/", $arr_parse_url['path'] );

            }

            $str_scheme_host = "{$arr_parse_url['scheme']}://{$arr_parse_url['host']}";

            return $str_scheme_host . $str_path_no_page;
        }
        return false;
    }
}