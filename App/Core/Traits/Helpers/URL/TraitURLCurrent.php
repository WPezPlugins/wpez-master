<?php
/**
 * Created by PhpStorm.
 */


namespace WPezMaster\App\Core\Traits\Helpers\URL;

trait TraitURLCurrent {

    /**
     * returns the current (WP-centric) URL
     *
     * @return string
     */
    protected function urlCurrent(){

        $str_home_url_no_slash = home_url();
        $arr_parse_url         = wp_parse_url( $str_home_url_no_slash );
        $str_current_url = "{$arr_parse_url['scheme']}://{$arr_parse_url['host']}" . add_query_arg( null, null );

        return $str_current_url;
    }
}