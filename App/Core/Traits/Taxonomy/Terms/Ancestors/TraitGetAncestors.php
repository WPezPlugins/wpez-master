<?php
/**
 * Created by PhpStorm.
 */


namespace WPezMaster\App\Core\Traits\Taxonomy\Terms\GetAncestors;

trait TraitGetAncestors {

    protected function getTermAncestors( $obj_term = false ) {

        $arr_bc = [];
        if ( $obj_term instanceof \WP_Term ) {

            $str_taxonomy              = $obj_term->taxonomy;
            $arr_bc[ $obj_term->slug ] = $obj_term;
            if ( $obj_term->parent != 0 ) {

                $arr_ancs = get_ancestors( $obj_term->term_id, $str_taxonomy, 'taxonomy' );

                if ( ! empty( $arr_ancs ) ) {

                    foreach ( $arr_ancs as $int_ann ) {

                        $obj = get_term( $int_ann, $str_taxonomy );
                        // TODO will slug be unique across multi taxs?
                        $arr_bc[ $obj->slug ] = $obj;
                    }
                }
            }

            return $arr_bc;
        }

        return $arr_bc;
    }
}