<?php

namespace WPezMaster\App;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezMaster\App\Setup\Admin\Cleanup\Posts\ClassCleanup;
use WPezMaster\App\Setup\Admin\Posts\CPT\ClassCPT;

use WPezMaster\App\MVC\Controllers\ClassController;

class ClassPlugin {

    protected $_str_post_type;
   //  protected $_str_shortcode_name;

    public function __construct() {

        $this->setPropertyDefaults();

        $this->CPT();

        $this->Cleanup();

    //    $this->addShortcode();
    }


    protected function setPropertyDefaults() {

        $this->_str_post_type      = 'TODO';
        $this->_str_shortcode_name = 'TODO';
    }


    protected function CPT() {

        $new = new ClassCPT( $this->_str_post_type );

    }

    protected function Cleanup() {

        $new = new ClassCleanup( $this->_str_post_type );

    }

    protected function addShortcode() {

        $new = new ClassController();

        $new->setPostType( $this->_str_post_type );
        $new->setDefaultWrapperTag('div');
        $new->setDefaultWrapperClass('wpez-TODO');
        $new->setDefaultWrapperIDSlug('wpez-TODO-');

        add_shortcode( $this->_str_shortcode_name, [ $new, 'shortcodeController' ] );
    }
}