<?php

namespace WPezMaster\App\Setup\Admin\Posts\CPT;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

use WPezMaster\App\Core\Hooks\Register\ClassRegister as HooksRegister;

use WPezMaster\App\Core\Admin\Posts\CPT\Labels\ClassEnglish as CPTLabels;
use WPezMaster\App\Core\Admin\Posts\CPT\Register\ClassRegister as CPTRegister;

use WPezMaster\App\Core\Admin\Posts\Columns\ClassAddDisplay;

class ClassCPT {

    protected $_str_post_type;

    protected $_arr_actions;
    protected $_arr_filters;

    public function __construct( $str_post_type = false ) {

        if ( ! is_string($str_post_type) ){
            return false;
        }
        $this->_str_post_type = $str_post_type;

        $this->setPropertyDefaults();

        $this->registerPostType();

        $this->postColumns();

        // this should be last
        $this->hooksRegister();
    }

    protected function setPropertyDefaults( ) {

        $this->_arr_actions   = [];
        $this->_arr_filters   = [];
    }



    protected function hooksRegister() {

        $new = new HooksRegister();;

        $new->loadActions( $this->_arr_actions );

        $new->loadFilters( $this->_arr_filters );

        $new->doRegister();
    }

    protected function registerPostType() {

        $str_single = 'MPezMaster';
        $str_plural = $str_single . 's';
        $arr_args   = [
            'public'              => true,            // default: false
            'exclude_from_search' => true,            // default: opposite of public (if this is not set)
            // 'publicly_queryable'	=> false,			// default: value of public
            'show_ui'             => true,            // default: value of public
            //  'show_in_nav_menus'		=> true, 			// dafault: value of public
            // 'show_in_menu'			=> true,			// default: value of show_ui
            'show_in_admin_bar'   => true,            // default: value of the show_in_menu argument
        ];

        // labels
        $new_labels = new CPTLabels();
        $new_labels->setSingular( $str_single );
        $new_labels->setPlural( $str_plural );
        $arr_labels = $new_labels->getLabels();

        // register CPT
        $new_reg = new CPTRegister();
        $new_reg->setPostType( $this->_str_post_type );
        $new_reg->setLabels( $arr_labels );
        $new_reg->setSupports( [ 'title', 'editor', 'author', 'thumbnail' ] );
        $new_reg->setArgs( $arr_args );
        $new_reg->setMenuPosition( 20 );


        // add_action('init', [$new_reg, 'registerPostType']);
        $this->_arr_actions[] = (object)[
            'hook'      => 'init',
            'component' => $new_reg,
            'callback'  => 'registerPostType',
            'priority'  => 10
        ];
    }


    protected function postColumns() {

        $new = new ClassAddDisplay();
        $new->setWPField( 'ID' );
        $new->setColumnTitle( 'ID' );
        $new->setAddColumnIndex( 1 );
        $new->setColumnWidth( '8%' );
        // $new->setEditPostLink(true);
        // $new->injectCustomColumnDisplay( [$this, 'zzz']);

        // add_filter( 'manage_' . $this->_str_post_type . '_posts_columns', [ $new, 'addColumn' ] );
        $this->_arr_filters[] = (object)[
            'hook'      => 'manage_' . $this->_str_post_type . '_posts_columns',
            'component' => $new,
            'callback'  => 'addColumn',
            'priority'  => 10
        ];

        // add_filter( 'manage_edit-' . $this->_str_post_type . '_sortable_columns', [ $new, 'makeSortable' ] );
        $this->_arr_filters[] = (object)[
            'hook'      => 'manage_edit-' . $this->_str_post_type . '_sortable_columns',
            'component' => $new,
            'callback'  => 'makeSortable',
            'priority'  => 10
        ];

        // add_action( 'pre_get_posts', [ $new, 'preGetPosts' ] );
        $this->_arr_actions[] = (object)[
            'hook'      => 'pre_get_posts',
            'component' => $new,
            'callback'  => 'preGetPosts',
            'priority'  => 10
        ];

        //add_filter( 'manage_' . $this->_str_post_type . '_posts_custom_column', [ $new, 'managePostsCustomColumnDisplay' ], 10, 2 );
        $this->_arr_filters[] = (object)[
            'hook'          => 'manage_' . $this->_str_post_type . '_posts_custom_column',
            'component'     => $new,
            'callback'      => 'managePostsCustomColumnDisplay',
            'priority'      => 10,
            'accepted_args' => 2

        ];

        // add_action('admin_footer', [ $new, 'styleColumn'] );
        $this->_arr_actions[] = (object)[
            'hook'      => 'admin_footer',
            'component' => $new,
            'callback'  => 'styleColumn',
            'priority'  => 10
        ];
    }

}