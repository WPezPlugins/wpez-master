<?php
/**
 * Created by PhpStorm.
 */

namespace WPezMaster\App\MVC\Controllers;

use WPezMaster\App\MVC\View\ClassView;

class ClassController {

    use \WPezMaster\App\Core\Traits\FilterTheContent\TraitFilterTheContent;

    protected $_str_post_type;

    protected $_bool_default_wrapper_active;
    protected $_str_default_wrapper_semantic_tag;
    protected $_str_default_wrapper_tag;
    protected $_str_default_wrapper_id_slug;
    protected $_str_default_wrapper_id;
    protected $_str_default_wrapper_class;


    public function __construct() {

        $this->setPropertyDefaults();

    }

    protected function setPropertyDefaults() {

        $this->_str_post_type = false;

        // TODO - setters
        $this->_bool_default_wrapper_active      = true;
        $this->_str_default_wrapper_semantic_tag = false;
        $this->_str_default_wrapper_tag          = false;
        $this->_str_default_wrapper_id_slug      = false;
        $this->_str_default_wrapper_id           = false;
        $this->_str_default_wrapper_class        = false;

    }

    protected function getAttsDefaults() {

        $arr = [
            'active'         => true,
            'post_type'      => $this->_str_post_type,
            'id'             => false,
            'wrapper_active' => $this->_bool_default_wrapper_active,

            'semantic_tag'    => $this->_str_default_wrapper_semantic_tag,
            // note: the view has it's own default: div
            'wrapper_tag'     => $this->_str_default_wrapper_tag,
            'wrapper_id_slug' => $this->_str_default_wrapper_id_slug,
            'wrapper_id'      => $this->_str_default_wrapper_id,
            'wrapper_class'   => $this->_str_default_wrapper_class
        ];

        return $arr;
    }


    public function shortcodeController( $atts ) {

        $arr_atts = shortcode_atts( $this->getAttsDefaults(), $atts );

        if ( $arr_atts['id'] === false || $arr_atts['active'] !== true ) {
            return '';
        }

        $mix_post = $this->getModel( $arr_atts['id'] );
        if ( ! $mix_post instanceof \WP_Post ) {
            return '';
        }
        $obj_post = $mix_post;

        if ( $this->_str_post_type != '*' && $obj_post->post_type != $this->_str_post_type ) {
            return '';
        }

        $new = new ClassView();

        if ( $arr_atts['wrapper_active'] === true ) {

            // we'll rely on the view to do the brunt of the validation. the view knows best
            if ( $arr_atts['semantic_tag'] !== false ) {

                $new->setWrapperSemanticTag( $arr_atts['semantic_tag'] );
            }

            if ( $arr_atts['wrapper_tag'] !== false ) {

                $new->setWrapperTag( $arr_atts['wrapper_tag'] );
            }

            if ( $arr_atts['wrapper_id'] !== false ) {

                $new->setWrapperID( $arr_atts['wrapper_id'] );

            } elseif ( $arr_atts['wrapper_id_slug'] !== false &&  $arr_atts['wrapper_id_slug'] != '0' ) {

                $new->setWrapperID( $arr_atts['wrapper_id_slug'] . $obj_post->ID );

            }

            if ( $arr_atts['wrapper_class'] !== false ) {

                $new->setWrapperClass( $arr_atts['wrapper_class'] );
            }

        } else {

            $new->setWrapperActive( false );
        }

        $str_content = $this->filterTheContent( $obj_post->post_content );

        $new->setContent( $str_content );

        return $new->getView();
    }


    protected function getModel( $id = false ) {

        return get_post( $id );

    }


    /**
     * If the post_type is set to '*' then the check for a given post_type will
     * be bypassed
     *
     * @param bool $str
     */
    public function setPostType( $str = false ) {

        // TODO - check against registers post types
        if ( is_string( $str ) ) {
            $this->_str_post_type = $str;

            return true;
        }

        return false;
    }

    public function setDefaultWrapperActive( $bool = true ) {

        if ( is_string( $bool ) ) {
            $this->_bool_default_wrapper_active = $bool;

            return true;
        }

        return false;
    }

    public function setDefaultWrapperSemanticTag( $str = false ) {

        if ( is_string( $str ) ) {
            $this->_str_default_wrapper_semantic_tag = $str;

            return true;
        }

        return false;
    }

    public function setDefaultWrapperTag( $str = false ) {

        if ( is_string( $str ) ) {
            $this->_str_default_wrapper_tag = $str;

            return true;
        }

        return false;
    }

    public function setDefaultWrapperIDSlug( $str = false ) {

        if ( is_string( $str ) ) {
            $this->_str_default_wrapper_id_slug = $str;

            return true;
        }

        return false;
    }

    public function setDefaultWrapperID( $str = false ) {

        if ( is_string( $str ) ) {
            $this->_str_default_wrapper_id = $str;

            return true;
        }

        return false;
    }

    public function setDefaultWrapperClass( $str = false ) {

        if ( is_string( $str ) ) {
            $this->_str_default_wrapper_class = $str;

            return true;
        }

        return false;
    }

}