## WPezPlugins: Master

__Ongoing aggregate of WPezPlugins Core classes, as well as serves as a boilerplate for plugin folder structure__ 



> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### OVERVIEW

The App / Core folder is our goto library. You won't need to change this code unless there's a bug, or similar.

The App / Setup folder is the custom code, that may or may not use the Core classes. The setup files in this repo are strictly for example / sample purposes. 

1. Fork this repo.

2. Rename the project folder, as well as wpez-master.php.

3. Updated the plugin details in wpez-master.php

4. Do a mass replace to update the namespace of all files.

5. Get to work.


### FAQ

__1 - Why?__

Less reinventing the wheel and more consistency is always a plus.


### HELPFUL LINKS

- TODO
 
 
### TODO

- Better documention of the individual classes in App / Core.

### CHANGE LOG

__-- 0.0.1__

- INIT - And away we go...


